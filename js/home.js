function clickNextDiv(pos) {
    var arr = document.getElementsByClassName("active");
    for (var i = 0; i < arr.length; i++) {
        arr[i].style.backgroundColor = "white";
    }
    arr[pos].style.backgroundColor = "black";
    var divName;
    if (pos == 0) {
        divName = "btnHome";
    }
    if (pos == 1) {
        divName = "btnPro";
    }
    if (pos == 2) {
        divName = "btnAbout";
    }
    if (pos == 3) {
        divName = "btnReg";
    }
    if (pos == 4) {
        divName = "btnContact";
    }
    var arrBtn = document.getElementsByClassName("btn");
    for (var index = 0; index < arrBtn.length; index++) {
        arrBtn[index].style.color = "antiquewhite";
    }
    var div = document.getElementById(divName);
    div.style.color = "steelblue";
}
// var current = "current";
// window.localStorage.setItem(current, 0);
// document.onkeydown = moveDown;
// // document.onkeydown = moveUp;
// function moveDown(){
//     var a = window.localStorage.getItem(current);
//     a = parseInt(a)  + 1;
//     window.localStorage.setItem(current, a);
//     clickNextDiv(a);
//     document.getElementById("btnPro").click();
// }
// function moveUp() {
//     current = parseInt(current) - 1;       
//     clickNextDiv(current);
//     document.getElementById("btnHome").click();
// }

// function MyScroll() {
//     var tagert = window.pageYOffset;
//     window.scrollTo(0,1000);
// }
// var body = document.getElementById("page");
// body.addEventListener("mousewhell", MyScroll); doi
function subcribe() {
    var name = document.getElementById("fullname");
    var email = document.getElementById("email");
    var feedback = document.getElementById("message");
    var valueDate = document.getElementById("date").value;
    var check = document.getElementById("sub").checked;
    if (name.value.length == 0 || name.value.toUpperCase().match("^[ A-ZẮẰẲẴẶĂẤẦẨẪẬÂÁÀÃẢẠĐẾỀỂỄỆÊÉÈẺẼẸÍÌỈĨỊỐỒỔỖỘÔỚỜỞỠỢƠÓÒÕỎỌỨỪỬỮỰƯÚÙỦŨỤÝỲỶỸỴ]+$") == null) {
        alert("Name is empty or have numbers");
        name.focus();
        return false;
    } else if (email.value.length == 0 || email.value.match("\\w+@\\w+[.]\\w+([.]\\w+)?") == null) {
        alert("Email is empty or don't have format \"name@domain.com\"");
        email.focus();
        return false;
    } else if (feedback.value.length <= 20) {
        alert("Messeage at least 20 character");
        feedback.focus();
        return false;
    } else if (!valueDate) {
        alert("Please choose your birthdate");
        return false;
    } else if (check == false) {
        alert("Please check \"Get all infomation update from website\"");
        return false;
    } else {
        alert("Your information will be send!");
        return true;
        window.location.reload();
    }
}

function divCurrent(divName) {
    var d = document.getElementById("page");
    if (divName == "btnHome") {
        d.style.marginTop = "0px";
        clickNextDiv(0);
    }
    if (divName == "btnPro")
    {
        d.style.marginTop = "-980px";
        clickNextDiv(1);
    }
    if (divName == "btnAbout") {
        d.style.marginTop = "-1960px";
        clickNextDiv(2);
    }
    if (divName == "btnReg") {
        d.style.marginTop = "-2940px";
        clickNextDiv(3);
    }
    if (divName == "btnContact") {
        d.style.marginTop = "-3920px";
        clickNextDiv(4);
    }
}
var show;

function loading() {
    show = setTimeout(showPage, 2000);
    var menu = document.getElementById("menu");
    var title = document.getElementById("titleLoad");
    for (var index = 3; index >= 0; index--) {
        if (index == 0) {
            title.innerHTML = "Welcome you  ♥";
            wait(1000);
            break;
        }
    }
    menu.style.display = "none";

}

function showPage() {
    document.getElementById("divLoad").style.display = "none";
    document.getElementById("page").style.display = "block";
    document.getElementById("menu").style.display = "block";
}

function wait(ms) {
    var start = new Date().getTime();
    var end = start;
    while (end < start + ms) {
        end = new Date().getTime();
    }
}
var pos = 0;
function nextDiv() {

    var d = document.getElementById("page");
    var a = parseInt(d.style.marginTop, 10);
    if (isNaN(a)) {
        d.style.marginTop = "0px";
    }
    if (window.event.wheelDelta > 0) {
        if (a >= 0) {
            d.style.marginTop =  "0px";
            return;
        }
        d.style.marginTop = a + 980 + "px";
        pos--;
        clickNextDiv(pos);
    } else {
        if (a <= -3920) {
            d.style.marginTop =  "0px";
            pos = 0;
            clickNextDiv(pos);
            return;
        }
        d.style.marginTop = a - 980 + "px";
        pos++;
        clickNextDiv(pos);
    }
}